<?php
/*
 * ************************************************************************
 * * Nombre del Archivo: ContainsRutValidator.php
 * * Autor: Mario Figueroa [mfigueroa@tmwk.cl]
 * * Fecha de Creación: 2/8/23 21:29
 * ***********************************************************************
 * * Copyright (c) 2023 Mario Figueroa
 * * Queda prohibida la distribución y uso no autorizado de este archivo.
 * * Para obtener más detalles, consulta el archivo LICENSE.md
 * ***********************************************************************
 */

namespace TMWK\RutBundle\Validator\Constraints;

use TMWK\RutBundle\Utilities\RutValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ContainsRutValidator extends ConstraintValidator
{
    public function __construct(private readonly RutValidator $rut, $strict = false)
    {
    }

    public function validate($value, Constraint $constraint): void
    {
        if (null === $value || '' === $value) {
            return;
        }

        if (!$this->rut->validate($value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', $value)
                ->addViolation();
        }
    }
}

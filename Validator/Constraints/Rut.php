<?php
/*
 * ************************************************************************
 *  * Nombre del Archivo: Rut.php
 *  * Autor: Mario Figueroa [mfigueroa@tmwk.cl]
 *  * Fecha de Creación: 2/8/23 21:34
 *  ***********************************************************************
 *  * Copyright (c) 2023 Mario Figueroa
 *  * Queda prohibida la distribución y uso no autorizado de este archivo.
 *  * Para obtener más detalles, consulta el archivo LICENSE.md
 *  ***********************************************************************
 */

namespace TMWK\RutBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class Rut extends Constraint
{
    public string $message = 'El rut %string% no es valido.';

    public function validatedBy(): string
    {
        return 'rut.validator';
    }
}
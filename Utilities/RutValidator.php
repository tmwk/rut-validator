<?php
/*
 * ************************************************************************
 *  * Nombre del Archivo: RutValidator.php
 *  * Autor: Mario Figueroa [mfigueroa@tmwk.cl]
 *  * Fecha de Creación: 2/8/23 21:15
 *  ***********************************************************************
 *  * Copyright (c) 2023 Mario Figueroa
 *  * Queda prohibida la distribución y uso no autorizado de este archivo.
 *  * Para obtener más detalles, consulta el archivo LICENSE.md
 *  ***********************************************************************
 */

namespace TMWK\RutBundle\Utilities;

/**
 * Class RutValidator
 * @package Tmwk\RutBundle\Utilities
 */
class RutValidator
{
    /**
     * @param $rut
     * @return bool
     */
    public function validate($rut): bool
    {
        if (trim($rut) == '') {
            return false;
        }

        $rut = self::removeFormat($rut);
        $sub_rut = substr($rut, 0, strlen($rut) - 1);
        $sub_dv = substr($rut, -1);
        $x = 2;
        $s = 0;
        for ($i = strlen($sub_rut) - 1; $i >= 0; $i--) {
            if ($x > 7) {
                $x = 2;
            }
            if (!is_numeric($sub_rut[$i])){
                return false;
            }
            $s += $sub_rut[$i] * $x;
            $x++;
        }
        $dv = 11 - ($s % 11);
        if ($dv == 10) {
            $dv = 'K';
        }
        if ($dv == 11) {
            $dv = '0';
        }
        if ($dv == $sub_dv) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $rut
     * @return string
     */
    public function setFormat($rut): string
    {
        if ($rut == 0) {
            return '';
        }

        $rut    = self::removeFormat($rut);
        $rutTmp = array();

        if (preg_match('/^([0-9]*)(.)+/', $rut, $rutTmp)) {
            return number_format($rutTmp[1], 0, '', '.') . '-' . $rutTmp[2];
        }
        return '';
    }

    /**
     * @param $rut
     * @return string
     */
    public function removeFormat($rut): string
    {
        return strtoupper(preg_replace('/\.|,|-/', '', $rut));
    }

    public function removeDotFormat($rut): string
    {
        $rut = $this->setFormat($rut);
        return str_replace('.', '', $rut);
    }
}